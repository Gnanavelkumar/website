import Head from 'next/head'

import Link from 'next/link'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <ul>
<li><Link href="https://website-5gqamkqi1-kgnanavel-vectonecom.vercel.app/communication">Communication</Link></li>
<li><Link href="/collaboration"><a>Collaboration</a></Link></li>
<li><Link href="/meeting">Meeting</Link></li>
</ul>
    </div>
  )
}
